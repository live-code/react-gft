import React from 'react';
import { useInterceptor } from './useInterceptor';

export const Interceptor: React.FC = () => {
  const { error } = useInterceptor();

  return error ?
    <div className="alert alert-danger">errore</div> :
    null
}
