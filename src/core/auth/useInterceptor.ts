import axios from 'axios';
import { getItem } from '../../shared/utils/localstorage.util';
import { Auth } from './auth';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

export const useInterceptor = () => {

  const [error, setError] = useState<boolean>(false);
  const history = useHistory();

  useEffect(() => {
    axios.interceptors.request.use(function (config) {
      setError(false);
      return {
        ...config,
        headers: {
          Authorizaton: 'Bearer ' + getItem<Auth>('auth')?.token
        }
      };
    }, function (error) {
      return Promise.reject(error);
    });

    axios.interceptors.response.use(function (response) {
      return response;
    }, function (error) {

      if (!axios.isCancel(error)) {
        // console.log(error)
        setError(true);
        history.push('login/signin')
      }
      return Promise.reject(error);
    });

  }, [history])

  return {
    error
  }

}
