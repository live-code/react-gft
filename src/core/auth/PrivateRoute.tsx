import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { isAuthorized } from './authentication.service';

export const PrivateRoute: React.FC<RouteProps> = (props) => {
  return <Route {...props} >
    {
      isAuthorized() ?
        props.children :
        <Redirect to="/login/signin" />
    }
  </Route>
}
