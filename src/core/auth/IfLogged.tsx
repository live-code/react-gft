import React from 'react';
import { Redirect } from 'react-router-dom';
import { isAuthorized } from './authentication.service';

export const IfLogged: React.FC = (props) => {
  return <>
    {
      isAuthorized() ?
        props.children :
        null
    }
  </>
}
