import axios from 'axios';
import { Auth } from './auth';
import { deleteItem, getItem, setItem } from '../../shared/utils/localstorage.util';

const basePath = 'http://localhost:3001/login';

export function signIn(username: string, password:string): Promise<Auth> {
  return axios.get<Auth>(`${basePath}?username=${username}&pass=${password}`)
    .then(res => {
      setItem('auth', JSON.stringify(res.data))
      return res.data;
    })
}

export function signOut() {
  deleteItem('auth')
}


export function isAuthorized(): boolean {
  return !!getItem('auth')
}

