import React  from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { signOut } from '../auth/authentication.service';
import { IfLogged } from '../auth/IfLogged';

export const NavBar: React.FC = () => {
  const history = useHistory()
  function signOutHandler() {
    signOut()
    history.push('/login/signin')
  }

  console.log('render')
  return (
    <div>
      <nav className="navbar navbar-expand navbar-light bg-light">
        <NavLink to="/login" className="navbar-brand">LOGO</NavLink>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">

            <li className="nav-item ">
              <NavLink to="/counter"  className="nav-link"  activeClassName="bg-warning">
                Counter
              </NavLink>
            </li>

            <li className="nav-item ">
              <NavLink to="/components"  className="nav-link"  activeClassName="bg-warning">
                uikit
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink to="/demo-context"  className="nav-link" activeClassName="bg-warning" exact>
                context
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink to="/users"  className="nav-link" activeClassName="bg-warning" exact>
                users
              </NavLink>
            </li>

            <IfLogged>
              <li className="nav-item ">
                <NavLink to="/tvmaze"  className="nav-link"  activeClassName="bg-warning">
                  tvmaze
                </NavLink>
              </li>
            </IfLogged>

            <li className="nav-item ">
              <NavLink to="/page1"  className="nav-link"  activeClassName="bg-warning">
                page 1
              </NavLink>
            </li>
            <li className="nav-item ">
              <NavLink to="/page2"  className="nav-link"  activeClassName="bg-warning">
                page 2
              </NavLink>
            </li>

            <li className="nav-item" onClick={signOutHandler}>
              <div className="nav-link">
                Quit
              </div>
            </li>

          </ul>
        </div>
      </nav>


    </div>
  )
};
