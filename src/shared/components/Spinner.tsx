import React from 'react';

interface SpinnerProps {
  theme?: 'success' | 'warning' | 'primary' ;
}

export const Spinner: React.FC<SpinnerProps> = ({ theme = 'primary'}) => {
  return <div className={`spinner-border text-${theme}`} />
}
