import React from 'react';

interface StaticMapProps {
  location: string;
  zoom?: number;
}
export const StaticMap: React.FC<StaticMapProps> = ({ location, zoom = 5}) => {
  return (
    <img
      width="100%"
      src={`https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=${location}&size=600,400&zoom=${zoom}`}
      alt=""/>
  )
}
