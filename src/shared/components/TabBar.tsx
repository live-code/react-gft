import React, { useState } from 'react';
import cn from 'classnames';

interface TabbarProps {
  data: any[];
  onTabClick: (item: any) => void;
}

export const Tabbar: React.FC<TabbarProps> = (props) => {
  const [active, setActive] = useState<any>(null);

  function tabClickHandler(item: any) {
    props.onTabClick(item);
    setActive(item);
  }

  return (
    <ul className="nav nav-tabs">
      {
        props.data.map(item => {
          return (
            <li key={item.id} className="nav-item" onClick={() => tabClickHandler(item)}>
              <div className={cn('nav-link', {'active': item.id === active?.id})} aria-current="page" >{item.title}</div>
            </li>
          )
        })
      }

    </ul>
  )
};
