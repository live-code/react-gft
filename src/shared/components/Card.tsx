import React from 'react';
import cn from 'classnames';

interface CardProps {
  title: string;
  type?: 'success' | 'failed';
  image?: string;
  icon?: string;
  onIconClick?: () => void
}


export const Card: React.FC<CardProps> = ({
  title, type, image, icon, onIconClick, children
}) => {

  return (
    <div className="card mb-2">
      {image && <img className="card-img-top" src={image} width="100%"/>}

      <div className={cn(
        'card-header',
        { 'bg-success text-white': type === 'success'},
        { 'bg-danger': type === 'failed'},
      )}>
        <div className="d-flex justify-content-between">
          <div>{title}</div>

          {icon && <div onClick={onIconClick}>
            <i className={icon} />
          </div>}
        </div>
      </div>
      {children && <div className="card-body">{children}</div>}
    </div>
  )
}
