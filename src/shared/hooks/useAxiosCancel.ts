import { useState } from 'react';

export const CANCEL_MSG = 'Operation canceled by the user.';

export function useAxiosCancel() {
  const [error, setError] = useState<boolean>(false)

  function cancelFn(e: any) {
    if (e.message !== CANCEL_MSG) {
      console.log('..')
    }
  }

  return {
    cancelFn,
    error,
    setError
  }
}
