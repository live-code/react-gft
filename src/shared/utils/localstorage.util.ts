export function setItem(key: string, value: string) {
  localStorage.setItem(key, value)
}

export function getItem<T>(key: string): T | null {
  const data = localStorage.getItem(key);
  return data ?  JSON.parse(data) : null;
}

export function deleteItem(key: string) {
  return localStorage.removeItem(key)
}
