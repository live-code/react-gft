import React, { lazy, Suspense, useEffect } from 'react';
// import { DemoComponents } from './pages/uikit/DemoComponents';
// import { DemoCounter } from './pages/counter/DemoCounter';
import { DemoUsers } from './pages/users/DemoUsers';
import { TvMaze } from './pages/tvmaze/TvMaze';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { NavBar } from './core/components/NavBar';
import { Spinner } from './shared/components/Spinner';
import { UserDetails } from './pages/users-details/UserDetails';
import { Login } from './pages/login/Login';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { useInterceptor } from './core/auth/useInterceptor';
import { Interceptor } from './core/auth/Interceptor';
import { DemoContext } from './pages/demo-context/DemoContext';

const DemoCounter = lazy(() => import('./pages/counter/DemoCounter'));
const DemoComponents = lazy(() => import('./pages/uikit/DemoComponents'));
const PageX1 = lazy(() => import('./pages/pageX/pageX1'));
const PageX2 = lazy(() => import('./pages/pageX/pageX2'));


function App() {


  return (
    <div className="m-2">
      <BrowserRouter>
        <Suspense fallback={<Spinner />}>


          <Interceptor />
          <Route path="*" component={NavBar} />

          <Switch>
            <PrivateRoute path="/tvmaze" exact>
              <TvMaze />
            </PrivateRoute>

            <Route path="/demo-context" component={DemoContext} />
            <Route path="/login" component={Login} />
            <Route path="/counter" component={DemoCounter} />
            <Route path="/components" component={DemoComponents} />

            <Route path="/page1" component={PageX1} />
            <Route path="/page2" component={PageX2} />

            <Route path="/users" exact>
              <DemoUsers />
            </Route>
            <Route path="/users/:userId">
              <UserDetails />
            </Route>

            <Route path="*">
              <Redirect to="/counter" />
            </Route>
          </Switch>
        </Suspense>
      </BrowserRouter>
    </div>
  )
}
export default App;

// https://rebrand.ly/990157
