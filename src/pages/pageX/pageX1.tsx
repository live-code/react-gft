import React  from 'react';
import { PageX } from './pageX';

const PageX1: React.FC = () => {
  return <PageX value={1} />
};

export default PageX1;
