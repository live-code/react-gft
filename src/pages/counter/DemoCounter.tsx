import React, { useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';

const DemoCounter: React.FC = (props) => {
  console.log(props)
  const [count, setCount] = useState<number>(11)

  function inc() {
    setCount(count + 1)
    // setCount(c => ++c)
  }

  const dec = () => {
    setCount(count - 1);
  }

  return (
    <>
      <h1>{count}</h1>
      <button onClick={dec}>-</button>
      <button onClick={inc}>+</button>
    </>
  )
};

export default DemoCounter;
