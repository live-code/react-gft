import React from 'react';
import { TvMazeList } from './components/TvMazeList';
import { TvMazeModal } from './components/TvMazeModal';
import { useTvMaze } from './hooks/useTvMaze';

export const TvMaze: React.FC = () => {
  const {
    series, activeSeries, actions
  } = useTvMaze();

  return (
    <div>
      <input
        type="text" className="form-control"
        onKeyDown={actions.keyHandler}
      />

      <TvMazeList
        data={series}
        onViewDetails={actions.showModal}
      />

      {
        activeSeries &&
          <TvMazeModal
            active={activeSeries}
            onClose={() => actions.showModal(null)}
          />}
    </div>

  )
};

