import React  from 'react';
import { Show, TVMazeResponse } from '../model/tvmaze-response';

interface TvMazeListProps {
  data: TVMazeResponse[];
  onViewDetails: (item: Show) => void
}
export const TvMazeList: React.FC<TvMazeListProps> = (props) => {

  return (
    <div style={{ display: 'flex' }}>
      {
        props.data.map(item => {
          return <div key={item.show.id}>
            {
              item.show.image ?
                <img src={item.show.image?.medium} width="100"/> :
                <div>placeholder</div>
            }
            <div>
              <button onClick={() => props.onViewDetails(item.show)}>View Details</button>
            </div>
          </div>
        })

      }
    </div>
  )
};
