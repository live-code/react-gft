import React  from 'react';
import { Show } from '../model/tvmaze-response';

interface TvMazeModalProps {
  active: Show | null;
  onClose: () => void;
}
export const TvMazeModal: React.FC<TvMazeModalProps> = (props) => {
  return (
    <div style={{ position: 'fixed', top: 0, right: 0, left: 0, bottom: 0, background: 'gray' }}>
      <div>
        <img src={props.active?.image?.medium} width="300"/>
      </div>
      {props.active?.name}
      {
        props.active?.genres.map(g => {
          return <li key={g}>{g}</li>
        })
      }
      <button onClick={props.onClose}>close</button>
    </div>
  )
};
