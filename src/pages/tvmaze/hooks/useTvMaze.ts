import React, { useState } from 'react';
import { Show, TVMazeResponse } from '../model/tvmaze-response';
import axios from 'axios';

export function useTvMaze() {
  const [series, setSeries] = useState<TVMazeResponse[]>([])
  const [active, setActive] = useState<Show | null>(null)

  function keyHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      console.log(e.currentTarget.value)
      axios.get<TVMazeResponse[]>('http://api.tvmaze.com/search/shows?q=' + e.currentTarget.value)
        .then(res => {
          setSeries(res.data);
        })
    }
  }

  function showModal(show: Show | null) {
    setActive(show)
  }

  return {
    series,
    activeSeries: active,
    actions: {
      showModal,
      keyHandler,
    }
  }
}
