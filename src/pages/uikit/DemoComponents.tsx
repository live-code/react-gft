import React, { useState } from 'react';
import { Card } from '../../shared/components/Card';
import { Spinner } from '../../shared/components/Spinner';
import { Tabbar } from '../../shared/components/TabBar';
import { StaticMap } from '../../shared/components/StaticMap';

interface Country {
  id: number;
  title: string;
  desc: string;
}

const initialCountries: Country[] = [
  { id: 1, title: 'Germany', desc: 'bla bla bla germany'},
  { id: 2, title: 'Japan', desc: 'bla bla bla Japan'},
  { id: 3, title: 'Spain', desc: 'bla bla bla Spain'},
]

export default function DemoComponents() {
  const [countries, setCountries] = useState<Country[]>(initialCountries);
  const [activeCountry, setActiveCountry] = useState<Country | null>(null);
  const [zoom, setZoom] = useState<number>(6);

  function addCountry() {
    setCountries([...countries, { id: 4, title: 'Austria', desc: 'sono l\'austria'}]);
  }

  function tabClickHandler(item: Country) {
    setActiveCountry(item)
  }

  function openUrl(url: string): void {
    window.open(url)
  }

  function viewAlert() {
    alert('hello')
  }

  return (
    <div className="m-2">
      <Tabbar data={countries} onTabClick={tabClickHandler} />

      { activeCountry && (
        <>
          <div className="p-4">{activeCountry.desc}</div>
          <StaticMap location={activeCountry.title} zoom={zoom} />
          <button onClick={() => setZoom(zoom - 1)}>-</button>
          <button onClick={() => setZoom(s => s + 1)}>+</button>
        </>
      )}

      <hr/>

      <button onClick={addCountry}>ADD</button>

      <Spinner />
      <Spinner theme="success" />
      <Spinner theme="primary" />
      <Spinner theme="warning" />


      <Card title="hello" icon="fas fa-upload" onIconClick={viewAlert} >
        <form>
          <input type="text" className="form-control"/>
          <input type="text" className="form-control"/>
          <input type="text" className="form-control"/>
          <input type="text" className="form-control"/>
        </form>
      </Card>

      <Card title="ok! ce l'hai fatta!" type="success" icon="fas fa-link"  onIconClick={() => openUrl('http://www.microsoft.com')} >
        sei stato bravo!
      </Card>

      <Card title="ok! hai fallito!" type="failed" image="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Neckertal_20150527-6384.jpg/1200px-Neckertal_20150527-6384.jpg">
        hai perso!
      </Card>
    </div>
  )
}
