import React from 'react';
import { UserList } from './components/UserList';
import { UserDetail } from './components/UserDetail';
import { useUsers } from './hools/useUsers';
import { UserFormControlled } from './components/UserFormControlled';


export const DemoUsers: React.FC = () => {

  const { error, users, actions, activeUserId } = useUsers();

  console.log('DemoUsers: render', users)
  return (<div>

    {error && <div className="alert alert-danger">errore</div>}

    {/*<UserForm onSubmit={actions.addHandler} />*/}
    <UserFormControlled onSubmit={actions.addHandler} />

    <div className="row">
      <div className="col">
        <UserList
          users={users}
          onItemClick={actions.setActiveUserId}
          onDelete={actions.deleteHandler}
        />
      </div>

      { activeUserId && <div className="col">
        <UserDetail userId={activeUserId}/>
      </div>}
    </div>
  </div>)
};
