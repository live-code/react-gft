import React  from 'react';
import { NavLink } from 'react-router-dom';
import { User } from '../model/user';

interface UserListProps {
  users: User[];
  onItemClick: (id: number) => void;
  onDelete: (id: number) => void;
}
export const UserList: React.FC<UserListProps> = (props) => {

  function onDeleteHandler(e: React.MouseEvent, userId: number) {
    e.stopPropagation();
    props.onDelete(userId)
  }

  return <>
    {
      props.users.map(u => {
        return (
          <NavLink to={`/users/${u.id}`} key={u.id}>
            <li
              onClick={() => props.onItemClick(u.id)}
            >
              {u.name} - {u.gender}
              <i className="fa fa-trash"  onClick={(e) => onDeleteHandler(e, u.id)} />
            </li>
          </NavLink>
        )
      })
    }
  </>
};
