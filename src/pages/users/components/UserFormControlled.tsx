import React, { useEffect, useRef, useState } from 'react';
import { User } from '../model/user';
import cn from 'classnames';

const initialState: Partial<User> = {
  name: '',
  username: '',
  gender: 'none'
};

interface UserFormProps {
  onSubmit: (formData: Partial<User>) => void;
}
export const UserFormControlled: React.FC<UserFormProps> = (props) => {
  const [formData, setFormData] = useState<Partial<User>>(initialState);
  const [dirty, setDirty] = useState<boolean>(false);

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {

    setFormData({
      ...formData,
      [e.currentTarget.name]: e.currentTarget.value
    });

    setDirty(true);
  }

  function onSubmitHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    setFormData(initialState);
    setDirty(false);
    props.onSubmit(formData)
  }

  const isNameValid = formData.name && formData.name.length > 3;
  const isUsernameValid = formData.username && formData.username.length > 3;
  const isGenderValid = formData.gender && formData.gender !== 'none';
  const isFormValid = isNameValid && isUsernameValid && isGenderValid;

  return (
    <form onSubmit={onSubmitHandler}>
      <input
        className={cn('form-control', { 'is-invalid': !isNameValid && dirty} )}
        type="text" value={formData.name} onChange={onChangeHandler} name="name"/>
      <input
        className={cn('form-control', { 'is-invalid': !isUsernameValid  && dirty} )}
        type="text" value={formData.username} onChange={onChangeHandler} name="username"/>

      <select value={formData.gender} onChange={onChangeHandler} name="gender">
        <option value="none">Select gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>
      <br/>
      <button type="submit" disabled={!isFormValid}>Add User</button>
    </form>
  )
};
