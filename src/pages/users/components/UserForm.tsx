import React, { useEffect, useRef } from 'react';
import { User } from '../model/user';

interface UserFormProps {
  onSubmit: (formData: Partial<User>) => void;
}
export const UserForm: React.FC<UserFormProps> = (props) => {
  const inputName = useRef<HTMLInputElement>(null);
  const inputUsername = useRef<HTMLInputElement>(null);
  const inputEmail = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputName.current?.focus()
  }, [])

  function addUserHandler() {
    if (inputName.current) {
      const isNameValid = inputName.current?.value.length > 3

      if (!isNameValid) {
        return;
      }
      props.onSubmit({
        name: inputName.current?.value,
        username: inputUsername.current?.value,
        email: inputEmail.current?.value,
      })
      inputName.current.value = '';
      inputName.current.focus()
    }
  }

  console.log('Form: render')
  const isNameValid = inputName.current && inputName.current?.value.length > 3

  return (
    <>
      <input type="text" ref={inputName} />
      <input type="text" ref={inputUsername} />
      <input type="text" ref={inputEmail} />
      <button onClick={addUserHandler} disabled={!isNameValid}>Add User</button>
    </>
  )
};
