import React, { useEffect, useState } from 'react';
import { User } from '../model/user';
import axios from 'axios';

interface UserDetailProps {
  userId: number;
}

export const UserDetail: React.FC<UserDetailProps> = ({ userId }) => {
  const [activeUser, setActiveUser] = useState<User | null>(null)

  useEffect(() => {
    axios.get<User>('http://localhost:3001/users/' + userId)
      .then((res) => {
        setActiveUser(res.data)
      })
  }, [userId]);

  return (
    <>
      <div>{activeUser?.name}</div>
      <div>{activeUser?.phone}</div>
    </>
  )
};
