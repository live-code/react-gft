import { useEffect, useState } from 'react';
import { User } from '../model/user';
import { CANCEL_MSG, useAxiosCancel } from '../../../shared/hooks/useAxiosCancel';
import axios from 'axios';

export function useUsers() {
  const [users, setUsers] = useState<User[]>([]);
  const [activeUserId, setActiveUserId] = useState<number | null>(null)
  const {cancelFn, error, setError } = useAxiosCancel();

  // init
  useEffect(() => {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    axios.get<User[]>('http://localhost:3001/users', {
      cancelToken: source.token
    })
      .then((res) => {
        setUsers(res.data)
      })
      // .catch(cancelFn)

    return () => {
      source.cancel(CANCEL_MSG);
    }
  }, [])


  function addHandler(formData: Partial<User>) {
   // const formData: Pick<User, 'name'> = { name: 'Fabio ' + Math.random() };
    axios.post<User>('http://localhost:3001/users', formData)
      .then(res => {
        setUsers([...users, res.data])
      })
      .catch(cancelFn)
  }

  function deleteHandler(id: number | undefined) {
    axios.delete(`http://localhost:3001/users/${id}`)
      .then(() => {
        setUsers(
          users.filter(u => u.id !== id)
        )
      })
      .catch(cancelFn)
  }

  return {
    error,
    users,
    activeUserId,
    actions: {
      addHandler,
      setActiveUserId,
      deleteHandler,
    }
  }
}
