import React, { createContext, useCallback, useContext, useState } from 'react';

const CounterContext = createContext<number | null>(null)

export function DemoContext() {
  console.log('------\nApp: render')
  const [data, setData] = useState(456)

  const add = useCallback((value: number) => {
    setData(value)
  }, [])
  return (
    <div className="comp">
      <CounterContext.Provider value={data}>
        <h3>Demo Context</h3>

        <button>-</button>
        <button>Random</button>
        <Dashboard onAdd={add} />
      </CounterContext.Provider>
    </div>
  );
}


interface DashboardProps {
  onAdd: (value: number) => void;
}
const Dashboard = React.memo((props: DashboardProps) => {
  console.log(' dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanel onAdd={props.onAdd}/>
    <RandomPanel />
  </div>
})


interface CounterProps {
  onAdd: (value: number) => void;
}
const CounterPanel: React.FC<CounterProps> = (props) => {
  const counter = useContext(CounterContext);
  console.log('  CounterPanel: render')
  return <div className="comp">
    Count:  {counter}
    <button onClick={() => props.onAdd(999)}>+</button>
  </div>
}

interface RandomPanelProps {
  onAdd?: (value: number) => void;
}
const RandomPanel = React.memo((props:RandomPanelProps ) => {
  console.log('  Random Panel: render')

  return <div className="comp">
    RandomPanel: abc
   {/* <button onClick={() => props.onAdd(999)}>+</button>*/}
  </div>
}
)
