import React  from 'react';
import { signIn } from '../../../core/auth/authentication.service';
import { useHistory } from 'react-router-dom';

export const SignIn: React.FC = () => {

  const history = useHistory();

  function signInHandler() {
    signIn('mario', '123')
      .then((res) => {
        // history.replace('/counter')
        history.push('/counter')
        console.log('redirect', res.token)
      })
    // redirect
  }

  return <>
    <h1>Signin</h1>
    <input type="text"/>
    <input type="text"/>

    <button onClick={signInHandler}>SignIn</button>
  </>
};
