import { Link, Redirect, Route, RouteComponentProps, useLocation, useParams } from 'react-router-dom';
import { SignIn } from './components/SignIn';
import { LostPass } from './components/LostPass';
import { Registration } from './components/Registration';

export function Login(props: RouteComponentProps) {
  console.log(props.match.path)
  const basePath = props.match.path;

  return (
    <div>

      <Route path={`${basePath}/:section`} component={BreadCrumbs}/>

      <hr/>
      <Route path={`${basePath}/signin`}>
        <SignIn />
      </Route>
      <Route path={`${basePath}/lostpass`}>
        <LostPass />
      </Route>
      <Route path={`${basePath}/registration`} component={Registration} />

      <Route path="*">
        <Redirect to={`${basePath}/signin`} />
      </Route>


      <hr/>
      <Link to="/login/signin">Sign In</Link> |
      <Link to="/login/lostpass">Lost pass</Link> |
      <Link to="/login/registration">Registration</Link> |
    </div>

  )
}

const BreadCrumbs = ({ match }: RouteComponentProps<{ section: string}>) => {
  return <div>
     login / {match.params.section}
  </div>
}
