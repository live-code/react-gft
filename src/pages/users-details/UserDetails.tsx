import React, { useEffect, useState } from 'react';
import { NavLink, useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import { User } from '../users/model/user';

export const UserDetails: React.FC = () => {
  const history = useHistory()
  const params = useParams<{ userId: string}>();
  const [data, setData] = useState<User | null>(null)

  useEffect(() => {
    console.log(params.userId)
    axios.get(`http://localhost:3001/users/${params.userId}`)
      .then(res => {
        setData(res.data)
      })
  }, [params.userId]);

  function backHandler() {
    history.goBack()
  }

  return (
    <div>
      { !data && <div>loading data...</div>}

      <h1>{data?.name}</h1>
      <h1>{data?.email}</h1>

      <button onClick={backHandler}>back history</button>
      <NavLink to="/users">back</NavLink>
    </div>
  )
};
